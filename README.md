# Prueba Técnica Full Stack Flutter Golang

Prueba tecnica como Full Stack Flutter y Golang developer para la empresa Cidenet S.A.S

## Aplicacion Movil

Para instalar la aplicacion es necesario ingresar a la ruta "cidenet_app\build\app\outputs\apk\release" e instalar en un dispositivo android el ejecutable "app-release.apk".

Si tiene instalado Flutter en su computador puede ejecutar la aplicacion corriendo los comandos:
1. Flutter pub get
2. Flutter install

## Aplicacion del Servidor

El servidor se desplego en Heroku por lo que para testearlo solo debe ingresar a "https://cidenet.herokuapp.com".

1. Para consultar la lista de empleados, solo debe ingresar a la url antes mencionada con la ruta "/empleado" al final.
2. Si desea crear desde el servicio POST del servidor un nuevo empleado, debe mandar un JSON con la informacion base del empleado a la url mencionada con la ruta "/empleado" al final.
